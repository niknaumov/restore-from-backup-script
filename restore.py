import argparse
import datetime
import subprocess
import requests
import json
import urllib


def get_param():
    parser = argparse.ArgumentParser(allow_abbrev=False)
    parser.add_argument('login_and_password', help="Enter login:password." +
                        "You can take it from hp.beget.com")
    parser.add_argument('domain', help="Enter domain" +
                        "which linked to directory")
    parser.add_argument('date', type=datetime.date.fromisoformat,
                        help="YYYY-MM-DD")
    parser.add_argument('-t', dest='time',
                        type=datetime.time.fromisoformat, help="HH:MM:SS")
    parser.add_argument('-c', '--compression', dest='compression',
                        type=int, default=0, help="Compression" +
                        "public_html_old if it is not eaqual=0")
    args = parser.parse_args()
    login, password = args.login_and_password.split(":")
    return login, password, args.domain, args.date, args.time


def get_list_backups(login, password):
    get_file_backup_list = requests.get('https://api.beget.com/api/backup/' +
                                        'getFileBackupList?login=' + login +
                                        '&passwd='+password +
                                        '&output_format=json')
    get_mysql_backup_list = requests.get('https://api.beget.com/api/backup/' +
                                         'getMysqlBackupList?login=' +
                                         login + '&passwd=' + password +
                                         '&output_format=json')
    if json.loads(get_file_backup_list.content).get('status') == "error":
        print(json.loads(get_file_backup_list.content))
        raise SystemExit
    content_file = json.loads(get_file_backup_list.content)
    # list of available files to restore:
    file_backups_list = content_file.get('answer').get('result')
    content_mysql = json.loads(get_mysql_backup_list.content)
    # list of available mysql to restore
    mysql_backups_list = content_mysql.get('answer').get('result')
    if not(len(mysql_backups_list) * len(file_backups_list)):
        print("count of site or db in backup is == 0")
        print("exit")
        raise SystemExit
    return file_backups_list, mysql_backups_list


# check list of backup. if it`s ok - return id of backup
def check_backup_list(file_backups_list, date, time):
    date = date.isoformat()
    if not(time is None):
        time = time.isoformat()
    time_list = []
    id_list = []
    for i in file_backups_list:
        if i.get('date').split()[0] == date:
            time_list.append(i.get('date').split()[1])
            id_list.append(i.get('backup_id'))
    if len(id_list) == 0:
        print("On", date, "we didn`t make backup")
        print("exit")
        raise SystemExit
    if len(id_list) > 1 and time is None:
        print("On", date, "we make more than 1 backup. Enter a time")
        print("exit")
        raise SystemExit
    if len(id_list) > 1:
        try:
            index_of_list = time_list.index(time)
            return id_list[index_of_list]
        except ValueError:
            print("incorrect time")
            print("exit")
            raise SystemExit
    return id_list[0]


# return comand for make dump from beget_db.
def get_dump_command(domain):
    subproc_command = "ssh nnaumov@support-console.beget "\
                        "\"echo \"/opt/beget/support/bin/beget_db\" | yii\""\
                        "\" ssh {}\" |  grep \"mysqldump -h localhost\"\
                        ".format(domain)
    beget_db = subprocess.run(subproc_command, stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE, encoding='utf-8',
                              shell=True)
    if beget_db.returncode != 0:
        print("beget_db:")
        print(beget_db.stdout)
        print(beget_db.stderr)
        print("exit")
        raise SystemExit
    return beget_db.stdout


# validate count of db
def validate_get_dump_command(beget_db):
    bd_array = beget_db.split('\n')
    if (len(bd_array) - 1) != 1:
        print("beget_db found more than 1 db")
        print(beget_db)
        print("exit")
        raise SystemExit
    return bd_array[0]


def check_free_space(login, password, domain):
    cust_info = requests.get('https://api.beget.com/api/user/getAccountInfo' +
                             '?login=' + login + '&passwd=' + password +
                             '&output_format=json')
    if json.loads(cust_info.content).get('status') == "error":
        print(json.loads(cust_info.content))
        print("exit")
        raise SystemExit
    content = json.loads(cust_info.content)
    subproc_du_com = "ssh nnaumov@support-console.beget \"echo \"du -m\"\""\
                     "\"| yii ssh {} \" | tail -1 ".format(login)
    du_subproc_login = subprocess.run(subproc_du_com, shell=True,
                                      stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE,
                                      encoding='utf-8')
    du_login = int(du_subproc_login.stdout.split()[0])
    plan_quota = content.get('answer').get('result').get('plan_quota')
    available_disk_size = plan_quota - du_login
    du_site_com = "ssh nnaumov@support-console.beget \"echo \"du -m\" | \""\
                  "\" yii ssh {} \" | tail -1 ".format(domain)
    du_subproc_site = subprocess.run(du_site_com, stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE, encoding='utf-8',
                                     shell=True)
    duSite = int(du_subproc_site.stdout.split()[0])

    print('size of site root directory:', duSite, 'mb')
    print('available dick size: ', available_disk_size, 'mb')
    if (available_disk_size < duSite):
        print("exit")
        raise SystemExit
    return available_disk_size


def make_dump(domain):
    print('mysqldump start')
    dump_command_subproc = "ssh nnaumov@support-console.beget "\
        "\"/home/nnaumov/scripts/restore_proj/make_dump.sh {}\"\
        ".format(domain)
    dump_sub = subprocess.run(dump_command_subproc, shell=True,
                              stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                              encoding='utf-8')
    print(dump_sub.stderr)
    if(dump_sub.returncode != 0):
        print("I can`t make dump")
        print(dump_sub.stdout)
        print("exit")
        raise SystemExit
    bd_name = dump_sub.stdout
    print("db_name: ", bd_name)
    print('mysqldump finished (success)')
    return bd_name


def get_directory(domain):
    subproc_pwd = "ssh nnaumov@support-console.beget \"echo \"pwd\"\""\
                  "\" | yii ssh {}\"".format(domain)
    pwd = subprocess.run(subproc_pwd, shell=True, stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE, encoding='utf-8')
    if pwd.returncode != 0:
        print(pwd.stdout)
        print(pwd.stderr)
        print("exit")
        raise SystemExit
    return pwd.stdout.rstrip()


def validate_get_directory(absolute_path):
    directories_arr = absolute_path.split("/")
    relate_path = ""
    for i in range(4, len(directories_arr)):
        relate_path += "/" + directories_arr[i]
    relate_path += "/"
    return relate_path


def rename_dir(login, absolute_path):
    now = datetime.datetime.now()
    time = now.strftime("%H:%M:%S")
    newPath = absolute_path + '_old_' + str(time)
    subproc_mv = "ssh nnaumov@support-console.beget \"echo \"mv {} {}\" | yii ssh {}\"\
                 ".format(absolute_path, newPath, login)
    mv = subprocess.run(subproc_mv, shell=True, stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE, encoding='utf-8')
    if (mv.returncode != 0):
        print(mv.stdout)
        print(mv.stderr)
        print('I can`t rename site directory')
        print("exit")
        raise SystemExit
    print("renamed ", absolute_path, " to ", newPath)


def make_request(backup_id, relate_path, db_name):
    path = '"paths":["' + relate_path + '"]'
    json_restor_file = "{\"backup_id\":" + str(backup_id) + "," + path + "}"
    base = '"bases":["' + db_name.replace('\n', '') + '"]'
    json_restor_mysql = "{\"backup_id\":" + str(backup_id) + "," + base + "}"
    return json_restor_file, json_restor_mysql


def do_request(login, password, json_restor_file, json_restor_mysql):
    file_coded = urllib.parse.quote(json_restor_file)
    mysql_coded = urllib.parse.quote(json_restor_mysql)
    str_request_restore_file = 'https://api.beget.com/api/backup/restoreFile?'\
                               'login=' + login + '&passwd'\
                               '=' + password + '&input_'\
                               'format=json&output_format=json&input_data'\
                               '=' + file_coded
    do_restore_file = requests.get(str_request_restore_file)
    str_request_restore_mysql = 'https://api.beget.com/api/backup/'\
                                'restoreMysql?'\
                                'login=' + login + '&passwd'\
                                '=' + password + '&input_'\
                                'format=json&output_format=json&input_data'\
                                '=' + mysql_coded
    do_restore_mysql = requests.get(str_request_restore_mysql)
    print(do_restore_file.content)
    print(do_restore_mysql.content)


if __name__ == "__main__":
    params = get_param()
    login = params[0]
    password = params[1]
    domain = params[2]
    date = params[3]
    time = params[4]
    file_backups_list, mysql_backups_list = get_list_backups(login, password)
    backup_id = check_backup_list(file_backups_list, date, time)
    beget_db = get_dump_command(domain)
    check_free_space(login, password, domain)
    db_name = make_dump(domain)
    check_free_space(login, password, domain)
    absolute_path = get_directory(domain)
    relate_path = validate_get_directory(absolute_path)
    rename_dir(login, absolute_path)
    json_restor_file, json_restor_mysql = make_request(backup_id,
                                                       relate_path, db_name)
    do_request(login, password, json_restor_file, json_restor_mysql)
