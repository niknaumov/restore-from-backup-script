from datetime import date, time
import unittest
import restore


several_db = "mysqldump -h localhost -u'lahbah_wp1' lahbah_wp1 -p'H0&X9^wfN' > lahbah_wp1_24-08.sql\nmysqldump -h localhost -u'lahbah_wp1' lahbah_wp1 -p'H0&X9^wfN' > lahbah_wp1_24-08.sql\n"
normal_db = "mysqldump -h localhost -u'lahbah_wp1' lahbah_wp1 -p'H0&X9^wfN' > lahbah_wp1_24-08.sql\n"
file_backups_list_normal,mysql_backups_list_normal = restore.get_list_backups('lahbah','Origin123')
file_backups_list_several_time = [{'backup_id': 276917946, 'date': '2021-08-09 04:18:26'},
{'backup_id': 276273021, 'date': '2021-08-09 23:01:15'}, {'backup_id': 275534839, 'date': '2021-08-07 20:48:08'}]
mysql_backups_list_several_time = [{'backup_id': 276917946, 'date': '2021-08-09 04:18:26'},
{'backup_id': 276273021, 'date': '2021-08-09 23:01:15'}, {'backup_id': 275534839, 'date': '2021-08-07 20:48:08'}]
file_backups_list_null = mysql_backups_list_null = []
normal_db="mysqldump -h localhost -u'lahbah_wp1' lahbah_wp1 -p'H0&X9^wfN' > lahbah_wp1_23-08.sql\n"


class TestRestore(unittest.TestCase):
    def test_get_list_backups_bad_pass(self):
        with self.assertRaises(SystemExit):
            restore.get_list_backups('lahbah','BadPasswor')

    def test_get_list_backups_bad_user(self):
        with self.assertRaises(SystemExit):
            restore.get_list_backups('ThisUserDoNotExist', 'BadPasswor')
  
    def test_check_backup_list_date_not_exist(self):
        with self.assertRaises(SystemExit):
            restore.check_backup_list(file_backups_list_normal, date(2020,1,1),None)

    def test_check_backup_list_several_date_without_time(self):
        with self.assertRaises(SystemExit):
            restore.check_backup_list(file_backups_list_several_time,  date(2021,8,9), None)

    def test_check_backup_list_several_date_with_incorrect_time(self):
        with self.assertRaises(SystemExit):
            restore.check_backup_list(file_backups_list_several_time,  date(2021,8,9), time(20,48,8))
    def test_check_backup_list_several_date_correct_time(self):
            self.assertEqual(restore.check_backup_list(file_backups_list_several_time,  date(2021,8,9), time(4,18,26)), 276917946)

    def test_check_backup_list_list_is_empty(self):
        with self.assertRaises(SystemExit):
            restore.check_backup_list (file_backups_list_null,  date(2021,8,9), None)
            
    #date(2021,8,19) - date available to restore
    def test_check_backup_listNormal(self):
        self.assertEqual(restore.check_backup_list(file_backups_list_normal, date(2021,8,19), None), 276917946)
            
    def test_get_directoryRaise(self):
         with self.assertRaises(SystemExit):
            restore.get_directory('incorrectdomain.domain.incorrect')
    def test_validate_get_directoryStandartPath(self):
        self.assertEqual(restore.validate_get_directory('/home/l/login/site/public_html'),'/site/public_html/')

    def test_validate_get_directoryNonStandartPath(self):
        self.assertEqual(restore.validate_get_directory('/home/l/login/site/dev1/main/test/web'),'/site/dev1/main/test/web/')

    def test_get_dump_commandIncorrectDomain(self):
        with self.assertRaises(SystemExit):
            restore.get_dump_command('incorrectdomain.domain.incorrect')

    # lahbah.online - site without database
    def test_get_dump_command_zero_db(self):
        with self.assertRaises(SystemExit):
            restore.get_dump_command('lahbah.online')

    def test_validate_get_dump_command_several_db(self):
        with self.assertRaises(SystemExit):
            restore.validate_get_dump_command(several_db)

    def test_validate_get_dump_command_normal_input(self):
        print(normal_db)
        self.assertEqual(restore.validate_get_dump_command(normal_db), "mysqldump -h localhost -u'lahbah_wp1' lahbah_wp1 -p'H0&X9^wfN' > lahbah_wp1_23-08.sql" )
    

